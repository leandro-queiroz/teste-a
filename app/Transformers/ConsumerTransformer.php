<?php
namespace App\Transformers;

use App\Models\Consumer;
use League\Fractal;

class ConsumerTransformer extends Fractal\TransformerAbstract
{
	public function transform(Consumer $consumer)
	{
	    return [
	        'consumer_id'      => (int) $consumer->id,
	        'name'   => $consumer->name,
	        'api_key'    =>  $consumer->api_key,
	        'created_at'    =>  $consumer->created_at->format('d-m-Y'),
	        'updated_at'    =>  $consumer->updated_at != null ? $consumer->updated_at->format('d-m-Y') : null
	    ];
	}
}