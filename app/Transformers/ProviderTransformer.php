<?php
namespace App\Transformers;

use App\Models\Provider;
use League\Fractal;

class ProviderTransformer extends Fractal\TransformerAbstract
{
	public function transform(Provider $provider)
	{
	    return [
	        'provider_id'      => (int) $provider->id,
	        'name'   => $provider->name,
	        'endpoint'    =>  $provider->endpoint,
	        'created_at'    =>  $provider->created_at->format('d-m-Y'),
	        'updated_at'    =>  $provider->updated_at != null ? $provider->updated_at->format('d-m-Y') : null
	    ];
	}
}