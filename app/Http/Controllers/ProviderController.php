<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use Illuminate\Http\Request;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\ProviderTransformer;

class ProviderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
    /**
     * GET /providers
     * 
     * @return array
     */
    public function index(){
        $paginator = Provider::paginate();
        $providers = $paginator->getCollection();
        $resource = new Collection($providers, new ProviderTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->fractal->createData($resource)->toArray();
    }

    public function show($id){
        $provider = Provider::find($id);
        $resource = new Item($provider, new ProviderTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function store(Request $request){
        
        //validate request parameters
        $this->validate($request, [
            'name' => 'bail|required|max:100',
            'Endpoint' => 'bail|required',
        ]);

        $provider = Provider::create($request->all());
        $resource = new Item($provider, new ProviderTransformer);
        return $this->fractal->createData($resource)->toArray();
    }

    public function update($id, Request $request){

        //validate request parameters
        $this->validate($request, [
            'name' => 'max:100',
            'Endpoint' => ''
        ]);

        //Return error 404 response if provider was not found
        if(!Provider::find($id)) return $this->errorResponse('provider not found!', 404);

        $provider = Provider::find($id)->update($request->all());

        if($provider){
            //return updated data
            $resource = new Item(Provider::find($id), new ProviderTransformer); 
            return $this->fractal->createData($resource)->toArray();
        }

        //Return error 400 response if updated was not successful        
        return $this->errorResponse('Failed to update provider!', 400);
    }

    public function customResponse($message = 'success', $status = 200)
    {
        return response(['status' =>  $status, 'message' => $message], $status);
    }
}