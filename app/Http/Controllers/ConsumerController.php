<?php

namespace App\Http\Controllers;

use App\Models\Consumer;
use Illuminate\Http\Request;
use League\Fractal;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Transformers\ConsumerTransformer;

class ConsumerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $fractal;

    public function __construct()
    {
        $this->fractal = new Manager();  
    }
     /**
     * @OA\Get(
     *     path="/consumers",
     *     summary="Retorna todas consumers",
     *     tags={"Consumer"},
     *     description="",
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso",
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="Nenhum registro encontrado",
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function index(){
        $paginator = Consumer::paginate();
        $consumers = $paginator->getCollection();
        $resource = new Collection($consumers, new ConsumerTransformer);
        $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
        return $this->fractal->createData($resource)->toArray();
    }

     /**
     * @OA\Get(
     *     path="/consumers/{ConsumerId}",
     *     summary="Busca consumer por ID",
     *     description="Retorna consumer buscado",
     *     operationId="getConsumerById",
     *     tags={"Consumer"},
     *     @OA\Parameter(
     *         description="ID do consumer a ser retornado",
     *         in="path",
     *         name="ConsumerId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="ID inválido"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Nenhum registro encontrado"
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function show($id){
        $consumer = Consumer::find($id);
        $resource = new Item($consumer, new ConsumerTransformer);
        return $this->fractal->createData($resource)->toArray();
    }


     /**
     * @OA\Post(
     *     path="/consumers",
     *     tags={"Consumer"},
     *     operationId="addConsumer",
     *     summary="Cria novo consumer",
     *     description="",
     *     @OA\RequestBody(
     *         description="Objeto do consumer a ser criado",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         ),
     *     ),
     *     @OA\RequestBody(
     *         description="Objeto do consumer a ser criado",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         )
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Inválido",
     *     ),
     *     security={}
     * )
     */
    public function store(Request $request){
        
        //validate request parameters
        $this->validate($request, [
            'name' => 'bail|required|max:100',
            'api_key' => 'bail|required',
        ]);

        $consumer = Consumer::create($request->all());
        $resource = new Item($consumer, new ConsumerTransformer);
        return $this->fractal->createData($resource)->toArray();
    }


    /**
     * @OA\Put(
     *     path="/consumers/{ConsumerId}",
     *     summary="Grava novo Consumer",
     *     description="",
     *     operationId="addConsumer",
     *     tags={"Consumer"},
     *     @OA\Parameter(
     *         description="ID do consumer a ser retornado",
     *         in="path",
     *         name="ConsumerId",
     *         required=true,
     *         @OA\Schema(
     *           type="integer",
     *           format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Sucesso"
     *     ),
     *     @OA\Response(
     *         response="400",
     *         description="ID inválido"
     *     ),
     *     @OA\Response(
     *         response="404",
     *         description="Nenhum registro encontrado"
     *     ),
     *     security={
     *       {"api_key": {}}
     *     }
     * )
     */
    public function update($id, Request $request){

        //validate request parameters
        $this->validate($request, [
            'name' => 'max:100',
            'api_key' => ''
        ]);

        //Return error 404 response if consumer was not found
        if(!Consumer::find($id)) return $this->errorResponse('consumer not found!', 404);

        $consumer = Consumer::find($id)->update($request->all());

        if($consumer){
            //return updated data
            $resource = new Item(Consumer::find($id), new ConsumerTransformer); 
            return $this->fractal->createData($resource)->toArray();
        }

        //Return error 400 response if updated was not successful        
        return $this->errorResponse('Failed to update consumer!', 400);
    }
}