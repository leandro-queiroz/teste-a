<?php

namespace App\Http\Controllers;

use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Services\Modules\SitefService;

class SitefController extends Controller
{

    use ApiResponser;

    public $sitefService;

    public function __construct(SitefService $sitefService)
    {
        $this->sitefService = $sitefService;
    }

    /**
     * Creates a payment with module sitef
     * 
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        $response = $this->sitefService->createPayment($request);

        if ($response['status']) {
            return $this->successResponse($response);
        }

        return $this->errorResponse($response, $response['code']);
    }
    
}
