<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *   title="API Módulo de Pagamentos",
     *   version="1.0",
     *   @OA\Contact(
     *     email="support@example.com",
     *     name="TFIN"
     *   )
     * )
     */
}
