<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Models\Transaction;

class HealthCheckController extends Controller
{
    use ApiResponser;
    /**
     * Create an transaction.
     *
     * @param Illuminate
     * @return Illuminate\Http\Response
     */
    public function check()
    {
        try {
            $statusSql = app('db')->connection('mysql');
            $statusRedis = app('redis')->exists('teste');
            $statusMongo = app('db')->connection('mongodb');
            
            return [
                'mysql'     => $statusSql != null ? true : false,
                'redis'     => $statusRedis != null ? true : false,
                'mongodb'   => $statusMongo != null ? true : false,
            ];
        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        } catch (\Exception $e) {
            return [
                'error' => $e->getMessage()
            ];
        }

    }

}
