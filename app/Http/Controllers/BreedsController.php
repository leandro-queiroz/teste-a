<?php

namespace App\Http\Controllers;

use App\Models\Breed;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

use App\Services\TheCatsApiService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class BreedsController extends Controller
{

    use ApiResponser;

    public $theCatsApiService;

    public function __construct(TheCatsApiService $theCatsApiService)
    {
        $this->theCatsApiService = $theCatsApiService;
    }


    /**
     * Return a listing of the breeds.
     * @param \App\Models\Breed $breed
     * @return \Illuminate\Http\Response
     */
    public function index($breed)
    {
        return $this->successResponse($breed->get());
    }


    /**
     * Return a breed or a listing of the breeds.
     * @param \App\Models\Breed $breed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function show($breed, $request)
    {
        return $this->successResponse($breed->findOrFail($request->id));
    }


    /**
     * Return a listing of the breeds by temperament.
     * @param \App\Models\Breed $breed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showByTemperament($breed, $request)
    {
        $breeds = $breed->where('temperament', 'like', "%{$request->temperament}%")->get();

        if (!count($breeds))
            return $this->errorResponse('Does not exist any instance of breed with the given temperament');

        return $this->successResponse($breeds);
    }


    /**
     * Return a listing of the breeds by origin.
     * @param \App\Models\Breed $breed
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function showByOrigin($breed, $request)
    {
        $breeds = $breed->where('origin', $request->origin)->get();

        if (!count($breeds))
            return $this->errorResponse('Does not exist any instance of breed with the given origin');

        return $this->successResponse($breeds);
    }


    public function createFrom

    // public function apiCrawler(Breed $model)
    // {

    //     $breeds = $this->theCatsApiService->getBreeds();

    //     $cats = [];
    //     foreach ($breeds as $breed) {
    //         $params = [
    //             'breed_id' => $breed->id,
    //             'limit' => 3
    //         ];
    //         $cats[$breed->name] = $this->theCatsApiService->getCats($params);
    //     }

    //     $cats['chapeu'] = $this->theCatsApiService->getCats(['category_ids' => [1], "limit" => 3]);

    //     $cats['oculos'] = $this->theCatsApiService->getCats(['category_ids' => [4], "limit" => 3]);
    // }

}
