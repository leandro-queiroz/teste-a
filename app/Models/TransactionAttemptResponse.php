<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class TransactionAttemptResponse extends Model
{

    /** 
     * Connection name.
     */
    protected $connection = 'mongodb';

    /** 
     * Collection name.
     */
    protected $collection = 'transaction_attempt_response';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid',
        'transaction_attempt_id',
        'data'
    ];

}