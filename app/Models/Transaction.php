<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Consumer;
use App\Models\TransactionStatus;


class Transaction extends Model
{
    /** 
     * Table name.
     */
    protected $table = 'TR002_Transaction';
    /** 
     * Primary key.
     */
    protected $primaryKey = 'transaction_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid',
        'consumer_id',
        'transaction_status_id',
        'webhook',
        'attempts',
        'limit_date',
        'billing_type',
        'amount',
        'description',
        'external_reference',
        'card_number',
        'card_cvv',
        'card_expiration_date',
        'card_holder_name',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];


    /**
     * Retorna o Consumer da Transaction
     */
    public function consumer()
    {
    	return $this->belongsTo(Consumer::class, 'consumer_id');
    }

    /**
     * Retorna o Status da Transaction
     */
    public function transactionStatus()
    {
    	return $this->belongsTo(TransactionStatus::class, 'transaction_status_id');
    }

}
