<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionStatus extends Model
{
    /**
     * Status ID of transactions
     */
    const PAID = 'PD';
    const UNPAID = 'UNPD';
    const AWAITING_PROCESSING = 'AWPROC';
    const PROCESSING_PAYMENT = 'PROCPYMNT';
    const PAYMENT_DATE_EXPIRED = 'PYMNTDATEEXP';
    const OVERPAYMENT_ATTEMPTS = 'OVPDATT';
    const GATEWAY_ERROR = 'GWERR';
    const MODULE_ERROR = 'MODERR';
    const GENERIC_ERROR = 'GENERR';

    /** 
     * Table name.
     */
    protected $table = 'TR002_TransactionStatus';
    /** 
     * Primary key.
     */
    protected $primaryKey = 'transaction_status_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'code',
        'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
