<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Breed extends Model
{

    /** 
     * Table name.
     */
    protected $table = 'breeds';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'origin',
        'temperament',
        'description'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
