<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class TransactionResponse extends Model
{

    /** 
     * Connection name.
     */
    protected $connection = 'mongodb';

    /** 
     * Collection name.
     */
    protected $collection = 'transaction_response';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'data'
    ];

}