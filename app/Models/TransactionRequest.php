<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class TransactionRequest extends Model
{

    /** 
     * Connection name.
     */
    protected $connection = 'mongodb';

    /** 
     * Collection name.
     */
    protected $collection = 'transaction_request';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid',
        'items',
        'api_key',
        'customer',
        'transaction',
    ];

}