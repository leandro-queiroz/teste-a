<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    /**
     * Provider IDs
     */
    const SITEF = 1;

    /** 
     * Table name.
     */
    protected $table = 'TR003_Provider';
    /** 
     * Primary key.
     */
    protected $primaryKey = 'provider_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'endpoint', 
        'active',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
