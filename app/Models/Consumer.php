<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{

    /**
     * Consumer IDs
     */
    const VETOR_API2 = 1;

    /** 
     * Table name.
     */
    protected $table = 'TR001_Consumer';
    /** 
     * Primary key.
     */
    protected $primaryKey = 'consumer_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'api_key',
        'active',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];
}
