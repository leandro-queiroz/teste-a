<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Provider;
class TransactionAttempt extends Model
{
    /** 
     * Table name.
     */
    protected $table = 'TR002_TransactionAttempt';
    /** 
     * Primary key.
     */
    protected $primaryKey = 'transaction_attempt_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'guid', 
        'provider_id',
        'sequence',
        'paid',
        'created_at',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Retorna o Provider da Attempt
     */
    public function provider()
    {
    	return $this->belongsTo(Provider::class, 'provider_id');
    }
}
