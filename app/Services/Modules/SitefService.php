<?php

namespace App\Services\Modules;

use Illuminate\Http\Request;
use App\Models\TransactionStatus;
use App\Services\TransactionService;
use App\Traits\ConsumesExternalServices;

class SitefService
{
    use ConsumesExternalServices;
    
    const PROVIDER_ID = 1;
    
    /**
     * secret key to sitef-module
     */
    public $key;

    /**
     * url to sitef-module
     */
    public $baseUri;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var TransactionStatus
     */
    private $transactionStatus;

    public function __construct(
        TransactionService $transactionService,
        TransactionStatus $transactionStatus
    )
    {
        $this->key = config('services.tef.key');
        $this->baseUri = config('services.tef.base_uri');
        $this->transactionService = $transactionService;
        $this->transactionStatus = $transactionStatus;        
    }

    /**
     * Handle response of sitef-microservice to insert a transaction
     * 
     * @param array $responseModule
     * @return array
     */
    private function handleResponseModulePayment($responseModule): array
    {

        if ($responseModule['status']) {
            return [
                "paid"          => $responseModule['data']['paid'],
                "data"          => $responseModule['data']['message'],
                "httpCode"      => $responseModule['code'],
                "statusCode"    => $responseModule['data']['paid'] ? $this->transactionStatus::PAID : $this->transactionStatus::UNPAID
            ];
        }

        return [
            "paid"          => false,
            "data"          => $responseModule['error'],
            "httpCode"      => $responseModule['code'],
            "statusCode"    => $this->transactionStatus::MODULE_ERROR
        ];

    }

    /**
     * Creates an transaction in sitef of pre payment 
     * 
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function createPayment(Request $request): array
    {
        $this->transactionService->logRequestData($request);

        $this->transactionService->getConsumerId($request);

        $this->transactionService->validateRequest($request);

        $this->transactionService->addTransactionToPoccessingRegistry();

        $httpresponseModule = $this->performRequest('POST', '/payment', $request->all());
        
        $responseModule = $this->handleResponseModulePayment($httpresponseModule);

        $this->transactionService->storeTransactionData($request, $responseModule['statusCode']);

        $this->transactionService->storeTransactionAttempt($httpresponseModule, self::PROVIDER_ID, $responseModule['paid']);

        $this->transactionService->removeTransactionFromPoccessingRegistry();

        return array_merge(['guid' => $this->transactionService->getGuid()], $httpresponseModule);
    }
} 
