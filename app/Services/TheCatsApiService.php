<?php

namespace App\Services;

// use App\Models\Consumer;
// use App\Models\Transaction;
// use App\Models\TransactionStatus;
// use App\Models\TransactionAttempt;
// use App\Models\TransactionRequest;
// use App\Models\TransactionAttemptResponse;
// use Illuminate\Support\Str;
// use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Validator;
// use Illuminate\Validation\ValidationException;
use App\Traits\ConsumesExternalServices;

class TheCatsApiService
{
    use ConsumesExternalServices;

    /**
     * secret key to sitef-module
     */
    public $key;

    /**
     * url to sitef-module
     */
    public $baseUri;

    /**
     * url to sitef-module
     */
    public $version;

    public function __construct(
        // TransactionService $transactionService,
        // TransactionStatus $transactionStatus
    )
    {
        $this->key = config('services.thecatsapi.key');
        $this->baseUri = config('services.thecatsapi.base_uri');
        $this->version = config('services.thecatsapi.version');
    }


    public function getBreeds()
    {
        $breeds = $this->performRequest('GET', "/{$this->version}/breeds");
        return $breeds['data'];
    }

    public function getCats(array $params)
    {
        $requestUrl = "/{$this->version}/images/search?";

        if (isset($params['limit'])) {
            $requestUrl.="limit={$params['limit']}";
        }

        if (isset($params['breed_id'])) {
            $requestUrl.= "&breed_id={$params['breed_id']}";
        }

        if (isset($params['category_ids']) && count($params['category_ids'])) {
            $requestUrl.= "&category_ids=" . implode('&category_ids=', $params['category_ids']);
        }

        $cats = $this->performRequest('GET', $requestUrl);

        if (!isset($cats['data'])) {
            dd('new exception');
        }

        return $cats['data'];
    }

}
