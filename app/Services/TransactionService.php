<?php

namespace App\Services;

use App\Models\Consumer;
use App\Models\Transaction;
use App\Models\TransactionStatus;
use App\Models\TransactionAttempt;
use App\Models\TransactionRequest;
use App\Models\TransactionAttemptResponse;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class TransactionService
{

    /**
     * Guid for transaction
     */
    private $guid;

    /**
     * Id of consumer application
     */
    private $consumer_id;

    /**
     * @var Consumer
     */
    private $consumer;

    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * @var TransactionStatus
     */
    private $transactionStatus;

    /**
     * @var TransactionAttempt
     */
    private $transactionAttempt;

    /**
     * @var TransactionAttemptResponse
     */
    private $transactionAttemptResponse;

    /**
     * @var TransactionRequest
     */
    private $transactionRequest;

    public function __construct(
        Consumer $consumer,
        Transaction $transaction,
        TransactionStatus $transactionStatus,
        TransactionAttempt $transactionAttempt,
        TransactionRequest $transactionRequest,
        TransactionAttemptResponse $transactionAttemptResponse
    )
    {
        $this->guid = Str::uuid()->toString();

        $this->consumer = $consumer;
        $this->transaction = $transaction;
        $this->transactionStatus = $transactionStatus;
        $this->transactionAttempt = $transactionAttempt;
        $this->transactionRequest = $transactionRequest;
        $this->transactionAttemptResponse = $transactionAttemptResponse;
    }

    /**
     * Deletes sensitive transaction data
     * 
     * @param array $request
     */
    private function hideSensitiveData(array $request): array
    {        

        // Ocultando api_key
        if(isset($request['api_key'])) {
            $secret = str_repeat("*", \strlen($request['api_key']) - 16);
            $request['api_key'] = substr_replace($request['api_key'], $secret, 8, -8);
        }

        // Ocultando o número do cartão
        if(isset($request['transaction']['credit_card']['number'])) {
            $secret = str_repeat("*", \strlen($request['transaction']['credit_card']['number']) - 10);
            $request['transaction']['credit_card']['number'] = substr_replace($request['transaction']['credit_card']['number'], $secret, 6, -4);
        }

        // Ocultando o cvv
        if(isset($request['transaction']['credit_card']['cvv'])) {
            $secret = str_repeat("*", \strlen($request['transaction']['credit_card']['cvv']));
            $request['transaction']['credit_card']['cvv'] = substr_replace($request['transaction']['credit_card']['cvv'], $secret, 0);        
        }

        return $request;
    }

    /**
     * Valide the consumer of transaction
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function getConsumerId(Request $request): int
    {
        $this->consumer_id = $this->consumer::where(['api_key' => $request['api_key'], 'active' => true])->firstOrFail()->consumer_id;
        return $this->consumer_id;
    }

    /**
     * Validate the request for a transaction
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function validateRequest(Request $request): void
    {
        $rules = [
            'api_key'                           => 'required|string',
            'transaction'                       => 'required',
            'transaction.async'                 => 'required|boolean',
            'transaction.anti_fraude'           => 'nullable|boolean',
            'transaction.webhook'               => 'nullable|url|max:255',
            'transaction.attempts'              => 'integer|max:255',
            'transaction.limit_date'            => 'nullable|date',
            'transaction.billing_interval'      => 'nullable|numeric|max:44640', //minutes 1 month
            'transaction.billing_type'          => 'required|string',
            'transaction.amount'                => 'required|numeric',
            'transaction.description'           => 'nullable|string',
            'transaction.external_reference'    => 'nullable|alpha_num',
        ];

        if ($request->input('transaction.async')) {

            $rules['transaction.async']             .= '|required';
            $rules['transaction.anti_fraude']       .= '|required';
            $rules['transaction.webhook']           .= '|required';
            $rules['transaction.attempts']          .= '|required';
            $rules['transaction.limit_date']        .= '|required';
            $rules['transaction.billing_interval']  .= '|required';
            
        }
        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails())
            throw new ValidationException($validator);
    }

    /**
     * Save the requisition in the mongodb
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function logRequestData(Request $request): void
    {
        $requestClone = array_merge(['guid' => $this->guid], $request->all());

        $requestClone = $this->hideSensitiveData($requestClone);

        $this->transactionRequest::create($requestClone);
    }

    /**
     * Parse data transaction to store on mysql
     * 
     * @param \Illuminate\Http\Request $request
     */
    private function parseTransactionRequestToRelational(Request $request): array
    {
        $requestData = $this->hideSensitiveData($request->all());

        $transaction = $requestData['transaction'];

        if (isset($transaction['credit_card'])) {
            $transaction['card_number'] = $transaction['credit_card']['number'] ?? null;
            $transaction['card_cvv'] = $transaction['credit_card']['cvv'] ?? null;
            $transaction['card_expiration_date'] = $transaction['credit_card']['expiration_date'] ?? null;
            $transaction['card_holder_name'] = $transaction['credit_card']['holder_name'] ?? null;
        }

        return $transaction;
    }

    /**
     * Add the transaction guid in the redis with the start time of processing
     * 
     * @param \Illuminate\Http\Request $request
     */
    public function addTransactionToPoccessingRegistry(): void
    {
        app('redis')->set($this->guid, date('Y-m-d h:i:s'));
    }

    /**
     * Remove the transaction guid in the redis after the processing
     * 
     */
    public function removeTransactionFromPoccessingRegistry(): void
    {
        app('redis')->del($this->guid);
    }

    /**
     * Save the transaction object on mysql
     * 
     * @param \Illuminate\Http\Request $request
     * @param string $statusCode
     * @return void
     */
    public function storeTransactionData(Request $request, string $statusCode): void
    {
        $transaction_status_id = $this->transactionStatus::where(['code' => $statusCode, 'active' => true])->firstOrFail()->transaction_status_id;

        $this->transaction::create(array_merge(
            [
                "guid" => $this->guid,
                "consumer_id" => $this->consumer_id,
                "transaction_status_id" => $transaction_status_id
            ],
            $this->parseTransactionRequestToRelational($request)
        ));
    }

    /**
     * Store the attempt for a transaction
     * 
     * @param \Illuminate\Http\Request $request
     * @param bool $paid
     * @param array $responseModule
     */
    public function storeTransactionAttempt(array $responseModule, int $providerId, bool $paid): void
    {
        $attemptsCount = $this->transactionAttempt::where('guid', $this->guid)->count();

        if (!$attemptsCount) {
            $attemptsCount = 1;
        }

        $transactionAttemptId = $this->transactionAttempt::create([
            'guid' => $this->guid,
            'provider_id' => $providerId,
            'sequence' => $attemptsCount,
            'paid' => $paid,
        ])->transaction_attempt_id;

        $this->logTransactionAttemptResponse($responseModule, $transactionAttemptId);
    }

    /**
     * Log response of module of transaction attempt on mongo db
     * 
     * @param array $responseModule
     * @param int $transactionAttemptId
     */
    private function logTransactionAttemptResponse(array $responseModule, int $transactionAttemptId): void
    {
        $responseModuleData = array_merge(['guid' => $this->guid, 'transaction_attempt_id' => $transactionAttemptId], ['data' => $responseModule]);

        $this->transactionAttemptResponse::create($responseModuleData);
    }

    /**
     * Change the transaction identification, use only to reprocessing
     * 
     * @param string $guid
     */
    public function setGuid(string $guid): void
    {
        $transaction_status_id = $this->transactionStatus::where(['code' => $this->transactionStatus::AWAITING_PROCESSING, 'active' => true])->firstOrFail()->transaction_status_id;

        $transaction = $this->transaction::where(['guid' => $guid, 'transaction_status_id' => $transaction_status_id])->count();

        if (!count($transaction))
            throw new \Exception("This transaction is not awaiting processing", 1);
            
        $this->guid = $guid;
    }

    /**
     * Get current transaction guid
     * 
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }
}
