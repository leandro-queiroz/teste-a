<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
    ];
});

// $factory->define(App\Models\Transaction::class, function (Faker $faker) {
//     return [
//         'guid' => $faker->text,
//         'consumer_id' => $faker->text,
//         'transaction_status_id' => $faker->text,
//         'webhook' => $faker->text,
//         'attempts' => $faker->text,
//         'limit_date' => $faker->text,
//         'billing_type' => $faker->text,
//         'amount' => $faker->text,
//         'description' => $faker->text,
//         'external_reference' => $faker->text,
//         'card_number' => $faker->text,
//         'card_cvv' => $faker->text,
//         'card_expiration_date' => $faker->text,
//         'card_holder_name' => $faker->text,
//         'created_at' => $faker->text,
//     ];
// });
