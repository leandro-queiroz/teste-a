<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('ConsumerTableSeeder');
        $this->call('ProviderTableSeeder');
        $this->call('TransactionStatusTableSeeder');

        // factory(App\Models\Transaction::class, 10)->create();
    }
}
