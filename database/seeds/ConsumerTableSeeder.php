<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConsumerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TR001_Consumer')->insert([
            'name' => 'Vetor-API2',
            'api_key' => '$2y$10$DIzpNkhzedRZ8Ft81djdNOG4HPvx1tx3JT6ZYlfkTRP7Wsr2jRpYS',
            'active' => '1',
            'created_at' => new DateTime()
        ]);
    }
}
