<?php

use Illuminate\Database\Seeder;

class ProviderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('TR003_Provider')->insert([
            'name' => 'SiTEF',
            'endpoint' => 'endpointsitef',
            'active' => '1',
            'created_at' => new DateTime()
        ]);
    }
}
