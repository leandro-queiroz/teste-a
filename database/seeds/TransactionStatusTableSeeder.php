<?php

use Illuminate\Database\Seeder;

class TransactionStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'name' => 'paid',
                'type' => 'sucess',
                'active' => '1',
                'code' => 'PD',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'unpaid',
                'type' => 'error',
                'active' => '1',
                'code' => 'UNPD',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'awaiting processing',
                'type' => 'confirmation',
                'active' => '1',
                'code' => 'AWPROC',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'processing payment',
                'type' => 'confirmation',
                'active' => '1',
                'code' => 'PROCPYMNT',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'payment date expired',
                'type' => 'error',
                'active' => '1',
                'code' => 'PYMNTDATEEXP',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'overpayment attempts',
                'type' => 'error',
                'active' => '1',
                'code' => 'OVPDATT',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'gateway error',
                'type' => 'error',
                'active' => '1',
                'code' => 'GWERR',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'module error',
                'type' => 'error',
                'active' => '1',
                'code' => 'MODERR',
                'created_at' => new DateTime(),
            ],
            [
                'name' => 'generic error',
                'type' => 'error',
                'active' => '1',
                'code' => 'GENERR',
                'created_at' => new DateTime(),
            ]
        ];

        DB::table('TR002_TransactionStatus')->insert($status);
    }
}
