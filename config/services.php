<?php

return [
    'thecatsapi' => [
        'key'       => env('THECATSAPI_SERVICE_KEY'),
        'version'   => env('THECATSAPI_SERVICE_VERSION'),
        'base_uri'  => env('THECATSAPI_SERVICE_BASE_URI')
    ]
];