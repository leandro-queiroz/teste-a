<?php

class ConsumerTest extends TestCase
{
    /**
     * /consumers [GET]
     */
    public function testShouldReturnAllConsumers(){

        $this->get("consumers", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'name',
                    'api_key',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
        
    }

    /**
     * /consumers/id [GET]
     */
    public function testShouldReturnConsumer(){
        $this->get("consumers/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'api_key',
                    'created_at'
                ]
            ]    
        );
        
    }

    /**
     * /consumers [POST]
     */
    public function testShouldCreateConsumer(){

        $parameters = [
            'name' => 'Kevin',
            'api_key' => 'hashdokevinqueeunaocrieimasessedevedarparatestar',
        ];

        $this->post("consumers", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'api_key',
                    'created_at',
                    'updated_at'
                ]
            ]    
        );
        
    }
    
    /**
     * /consumers/id [PUT]
     */
    public function testShouldUpdateConsumer(){

        $parameters = [
            'name' => 'Kevin2',
            'api_key' => 'hashdokevinqueeunaocrieimasessedevedarparatestar2',
        ];

        $this->put("consumers/2", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'api_key',
                    'created_at',
                    'updated_at'
                ]
            ]    
        );
    }

}