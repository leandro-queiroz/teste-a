<?php

class ProviderTest extends TestCase
{
    /**
     * /providers [GET]
     */
    public function testShouldReturnAllProviders(){

        $this->get("providers", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            'data' => ['*' =>
                [
                    'name',
                    'Endpoint',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);
        
    }

    /**
     * /providers/id [GET]
     */
    public function testShouldReturnProvider(){
        $this->get("providers/1", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'Endpoint',
                    'created_at'
                ]
            ]    
        );
        
    }

    /**
     * /providers [POST]
     */
    public function testShouldCreateProvider(){

        $parameters = [
            'name' => 'SiTef',
            'Endpoint' => 'endpoint.com.br/api/action',
        ];

        $this->post("providers", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'Endpoint',
                    'created_at',
                    'updated_at'
                ]
            ]    
        );
        
    }
    
    /**
     * /providers/id [PUT]
     */
    public function testShouldUpdateProvider(){

        $parameters = [
            'name' => 'SiTef2',
            'Endpoint' => 'endpoint.com.br/api/action2',
        ];

        $this->put("providers/2", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            ['data' =>
                [
                    'name',
                    'Endpoint',
                    'created_at',
                    'updated_at'
                ]
            ]    
        );
    }

}